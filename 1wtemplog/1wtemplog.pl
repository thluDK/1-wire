#!/usr/bin/perl -w

use strict;
use Config::General::Extended;
use Getopt::Long;
use Proc::Daemon;
use Data::Dumper;
use File::Slurp;

use owsensor;

my $conffile = "/home/thomas/projekter/1-wire/1wtemplog/1wtemplog.ini";
my $debuglevel = 0;
my $daemon = 0;

## command line options
GetOptions (
    'conffile=s' => \$conffile,
    'DEBUG' => \$debuglevel,
    'daemon' => \$daemon);

## Setup debug print
my $DEBUG;
if($debuglevel){
    $DEBUG = *STDOUT;
}else{
    open ($DEBUG , '>/dev/null' );
}

print $DEBUG "Conffile: $conffile\n";

## Read configuration file.
my $cfg = Config::General->new(-ConfigFile     => $conffile,
			       -ExtendedAccess => 1);

my $sensors = owsensor->new($cfg->value("sensorconf"));

foreach my $sensor ($sensors->listSensors()) {
    print "$sensor: ".$sensors->read($sensor)."\n";
}

## Catch signals to top whileloop
my $continue = 1;
$SIG{INT} = sub { $continue = 0 };
$SIG{TERM} = sub { $continue = 0 };


while($continue){

    $sensors->RRDupdateAll();
    sleep 1;
}


print $DEBUG "!!!END!!!\n";
END;
