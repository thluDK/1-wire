#!/usr/bin/perl -w

package owsensor;

use strict;
use Config::General::Extended;
use File::Slurp;
use RRDTool::OO;

sub new {

    my ($class_name, $sensorConfFile) = @_;
    
    # Create class
    my $self = {};
    bless ($self, $class_name);
    
    # Read configuration file
    $self->{cfg} = Config::General->new(-ConfigFile      => $sensorConfFile,
					-ExtendedAccess  => 1);
    
    
    # open RRD files
    $self->RRDopen();
    

    return $self;

}

# return list of sensor names
sub listSensors {

    my ($self) = @_;
    
    my @sensors;

    foreach my $sensor ($self->{cfg}->keys()) {
	next if ($sensor eq "GLOBAL") ;
	push(@sensors, $sensor);
    } 
    
    return @sensors;
}


# get configuration value
sub confVal {
    
    ## Get passed arguments
    my ($self, $sensor, $arg) = @_;
    
    ## is argument set in sensorconfig?
    return $self->{cfg}->obj($sensor)->value($arg) if $self->{cfg}->obj($sensor)->exists($arg);

    ## Get global value from config
    return $self->{cfg}->obj("GLOBAL")->value($arg) ;
    
    
}

## Read sensor value (String)
sub read {
    
    ## Get passed arguments
    my ($self, $sensor) = @_;
    
    my $file = $self->confVal($sensor, "owfs")."/".$self->confVal($sensor, "address")."/".$self->confVal($sensor, "value"); 
    
    return read_file($file) =~ s/^\s+|\s+$//rg; 
    
    
}

## TODO: get values from conffile
## Create RRD file
sub RRDcreate {

    my ($self, $sensor) = @_;
    
    my $samplerate = $self->confVal($sensor, "samplerate");
    my $heardbeat  = $samplerate*5;
    my $min        = $self->confVal($sensor, "min");
    my $max        = $self->confVal($sensor, "max");
    my $step1      = 1;
    my $rowes1     = int(2*31536000/($samplerate*$step1));
    my $step2      = int(3600/$samplerate);
    my $rowes2     = int(10*31536000/($samplerate*$step1));



    $self->{RRD}->{$sensor}->create(
	step        => $samplerate,  
	data_source => { name      => $sensor,
			 type      => "GAUGE",
			 min       => $min,
			 max       => $max },
	archive     => { rows      => $rowes1,
			 cpoints   => $step1,
			 cfunc     => 'AVERAGE'},
	archive     => { rows      => $rowes2,
			 cpoints   => $step2,
			 cfunc     => 'AVERAGE'}
	);
    

}

## Open reference to all RRD files. Create file if not found
sub RRDopen {

    my ($self) = @_;

    $self->{RRD} = {};
    $self->{RRDtime} = {};


    foreach my $sensor ($self->listSensors()) {
	
	my $rrdfile = $self->confVal($sensor, "rrddir").$sensor.".rrd";

	# open RRD object
	$self->{RRD}->{$sensor} = RRDTool::OO->new( file => $rrdfile );
	
	# Create file if not found
	if (!-f $rrdfile) {
	    $self->RRDcreate($sensor);    
	}

        # init time for last update
	$self->{RRDtime}->{$sensor} = 0;

    }
}


## update rrdfile vith value from sensor. skip if last updat is less then 'steptime' ago
sub RRDupdate {
   
    my ($self, $sensor) = @_;
    
    
    if($self->{RRDtime}->{$sensor} + $self->confVal($sensor, "samplerate") < time() ) {
	## Update
	$self->{RRD}->{$sensor}->update($self->read($sensor));

	# set time of last update to now
	$self->{RRDtime}->{$sensor} = time();
    }
    


}

sub RRDupdateAll {
    
    my ($self) = @_;

    foreach my $sensor ($self->listSensors()) {
	$self->RRDupdate($sensor);

    }

}


1;
