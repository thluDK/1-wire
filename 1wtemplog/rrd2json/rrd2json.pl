#! /usr/bin/perl

use RRDs;
use Data::Dumper;
use CGI qw/:standard/;
use List::Util qw(max min);

#$rrd_dir = '/var/www/mrtg/';
$rrd_dir = '../rrdfiles/';

#
# Get RRD data from the file $rrd for the $cf Consolidation Function
# with resolution $res seconds, for the day up to $end
#
sub getRRData($$$@) {
    my ($rrd, $cf, $numpoints, @ds) = @_;

    my $rrd_info = RRDs::info $rrd;
    my @rrd_data = {};

    $step = $rrd_info->{'step'};
    $last = $rrd_info->{'last_update'};
    my $i;
    for ($i = 0; exists($rrd_info->{'rra[' . $i . '].cf'}) && $rrd_info->{'rra[' . $i . '].cf'} ne $cf; $i++) {
        ;
    }


    if ($rrd_info->{'rra[' . $i . '].cf'} eq $cf) {
        my $rows = $rrd_info->{'rra[' . $i . '].rows'};
        if (defined($numpoints) && $numpoints < $rows) {
            $rows = $numpoints;
        }
        my $start = $last - ($step * $rows);
        @rrd_data = RRDs::fetch $rrd, $cf, "--resolution", $step, "--end", $last - $step, "--start", $start;

        my @all_ds = @{$rrd_data[2]};
        
        # indexes into the data series
        my @ds_idx = (0 .. $#all_ds);

        # whether to copy just selected fields from each row (using @ds_idx)
        my $selected_fields = 0;

        if ($#ds >= 0) {

            # if called with a set of data series, get all the ds names, and compare them
            # to the list of ds names passed in via the @ds parameter collect all the
            # indexes into @all_ds for elements matching elements in @ds, so that we can
            # pull the selected fields from the data as we filter it later

            $selected_fields = 1;
            @ds_idx = map { my $v = $_; my $j = -1; map { $j++; /^${v}$/ ? $j : (); } @all_ds } @ds;
        }
        
        #
        # whether to copy rows into the output or not
        #
        my $copy = 0;

        #
        # array to collect filtered data
        #
        my @data = ();

        #
        # loop through the data, and filter any leading rows full of undefined values,
        # then copy subsequent rows into the @data array - filtering out just the
        # selected fields if necessary
        #
        foreach my $d (@{$rrd_data[3]}) {
            $copy++
                if (grep { defined($_)} @{$d});
                
            if ($copy > 0) {
                my @flds = @{$d}[@ds_idx];
                push @data, \@flds;

            } else {
                $start += $step;
            }
        }
        
        
        $rrd_data[3] = \@data;
        $rrd_data[0] = $start;
        if ($selected_fields) {
            my @names = map { $rrd_data[2][$_] } @ds_idx;
            $rrd_data[2] = \@names;
        }
    }
    return @rrd_data;
}

#
# Dump the RRD data as JSON
# start is the start time (Unix time)
# stepsize is the interval between entries (in seconds)
# dsnames is an array of the names of the data sources
# numpoints is the number of entries in the data array
# data is an array of arrays; each array represents the data source values at a point in time
#
sub makeJSON($$$$) {
    my ($start,$step,$names,$data) = @_;
    my $dsnames = '"' . join ('", "', @$names) . '"';
    my $num = $#$data + 1;
    
    # convert start and step to milliseconds
    $start *= 1000;
    $step *= 1000;
    
    my $max = $$data[0][0];
    my $min = $max;

    print <<JSON
Content-type: application/json

{
    "start" : ${start}, 
    "stepsize" : ${step},
    "dsnames" :  [ ${dsnames} ],
    "num" : ${num},
    "data": \[
JSON
;
    #
    # map the RRD data into a JSON array of arrays
    #
    foreach my $line (@$data) {
        $max = max $max, @$line;
        $min = min $min, @$line;
        print "        [" . join(", ", map { (!defined($_) || $_ eq '') ? 'null' : sprintf("%.1f", $_); } @$line) . "],\n";
    }
    print <<JSON2
    ],
    "max" : ${max},
    "min" : ${min}
}
JSON2
;
}


#
#
my $rrd = ude;
my @ds = ("ude");
my $numpoints = undef;
my $cf = 'AVERAGE';

# for debugging
#printf("Content-type: text/plain\r\n\r\n");

	
@ds = split(/,/, $1)
    if (param('ds') =~ /^(\w+(,\w+)*)$/);
$numpoints = $1
    if (param('num') =~ /^(\d+)$/);
$cf = $1
    if (param('cf') =~ /^(\S+)$/);

$rrd = $rrd_dir . $1 . '.rrd'
    if (path_info() =~ /^([-\/\w]+)(\.rrd)?$/);


my ($start, $step, $names, $data) = getRRData($rrd, $cf, $numpoints, @ds);
makeJSON($start, $step, $names, $data);
