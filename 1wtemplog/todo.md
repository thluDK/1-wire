TODO
====

# Error handling

## En sensor defineret i sensorconf findes ikke i OWFS

## Kan ikke skrive til RRD filer / Ikke adgang til mappen



# Export data til graf

## Knapper under graf til tænd/sluk af grafer

## Gruppere målinger


# Hvilket format skal RRD filer have?
Undersøge hvad jeg brugte sidst og sammenligne med hvad Anders og Kasper har lavet.

#Fra rumtemp
	       --step 300                         
               --no-overwrite                     
	       DS:temp:GAUGE:1800:100:5000        
               DS:setpoint:GAUGE:1800:100:5000    
               RRA:AVERAGE:0.5:1:420480           
               RRA:AVERAGE:0.5:12:87600
alle data 4 år
gennemsnit for hver time i 10 år
fylder 4Mb pr. datapunkt


# RRD opsætning

Alle målinger gemmes 2 år
Gennemsnit fra hver time gemmes 10 år

* en datasource (DS) pr rrdfil
* --step 60 // samplerate fra sensorconf
* --no-overwrite // do not override if file exists
*  DS:ds-name:GAUGE:heartbeat:min:max
   ds-name = sensornavn
   heardbeat = 5*samplerate
   min fra sensorconf. Sanity check af måling
   max fra sensorconf. Sanity check af måling
*  RRA:AVERAGE:xff:steps:rows // Alle data i 2 år
   xff = 0.5 // antalen af data i perioden som skal være valide
   step = 1 // antal samplerate pr. periode
   rows = 2*<yearInSec>/(samplerate*step)
*  RRA:AVERAGE:xff:steps:rows // gennemsnit pr time i 10 år
   xff = 0.5 // antalen af data i perioden som skal være valide
   step = 3600/samplerate(INT) // antal samplerate pr. periode
   rows = 10*<yearInSec>/(samplerate*step)
   
### Datamængde
samplerate = 300 (5min) : 10M 
samplerate = 60  (1min) : 50M 
samplerate = 5   (5sec) : 600M 

# Rumtemperatur fra Danfoss termostater.

## Format af RRD filer skal være det samme som ow målinger


# Hardware

## Teste lang ledning til teknikskab
Alternativt Rpi i teknikskab.

## Kasse til ow driver og til danfoss læser.


