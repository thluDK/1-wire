$(function() {
    var seriesOptions = [],
    yAxisOptions = [],
    seriesCounter = 0,
    names = ['Stue', 'Ude', 'Soveværelse'],
    colors = Highcharts.getOptions().colors;

    $.each(names, function(i, name) {

	$.getJSON('data/testdata.php?starttemp='+ name.toLowerCase(),function(data) {

	    seriesOptions[i] = {
		name: name,
		data: data
	    };

	    // As we're loading the data asynchronously, we don't know what order it will arrive. So
	    // we keep a counter and create the chart when all the data is loaded.
	    seriesCounter++;

	    if (seriesCounter == names.length) {
		createChart();
	    }
	});
    });



    // create the chart when all data is loaded
    function createChart() {

	$('#container').highcharts('StockChart', {
	    chart: {
	    },

	    rangeSelector: {
		buttons: [{
		    type: 'month',
		    count: 1,
		    text: '1m'
		}, {
		    type: 'week',
		    count: 1,
		    text: '1w'
		}, {
		    type: 'day',
		    count: 2,
		    text: '2d'
		}, {
		    type: 'day',
		    count: 1,
		    text: '1d'
		}, {
		    type: 'ytd',
		    text: 'YTD'
		}],
		selected: 3	
	    },

	    yAxis: {
//		labels: {
//		    formatter: function() {
//			return (this.value > 0 ? '+' : '') + this.value + '%';
//		    }
//		},
		plotLines: [{
		    value: 0,
		    width: 2,
		    color: 'silver'
		}]
	    },
	        
//	    plotOptions: {
//		series: {
//		    compare: 'percent'
//		}
//	    },
	        
	    tooltip: {
		pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
		valueDecimals: 2
	    },
	        
	    series: seriesOptions
	});
    }

});
