<?PHP
error_reporting(E_ALL);
ini_set("display_errors", "true");

function getGet($var){
  
  if(isset($_GET[$var])) {
      return escapeshellcmd($_GET[$var]);
    }else{
      return null;
    }
}  

// /home/www/thlu.dk/hems.png DEF:vname=hems.rrd:hems:AVERAGE LINE:vname#0000FF

$plot = getGet("plot");
$days = getGet("len");

$width = 1000;
$hight = 300;

$step = intval(86400*$days/$width);

$step = getGet("step");

$start = "end-$days"."d";

$comment = $plot." - ".$step;

$rrdfile = "/home/thomas/projekter/1-wire/1wtemplog/rrdfiles/$plot.rrd";

$command = "rrdtool graph - -a PNG -s$start -w$width -h$hight DEF:vname=$rrdfile:$plot:AVERAGE:step=$step LINE:vname#0000FF COMMENT:'$comment'";

//echo $command;
//exit(0);

header('Cache-Control: max-age=60');
header('Content-Type: image/png');

passthru($command);


?>