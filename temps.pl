use OWNet;
use RRDs;
use XML::Simple; #libxml-simple-perl
use Data::Dumper;
use strict;
use File::Find;
use Cwd;
#use strict;

my $dir = getcwd;
my $xmlCfgDir = "/home/anders/temp/sensors";
my $rrdCfgDir = "/home/anders/temp/rrds";
my $sensorAliasFile = "$dir/sensorAlias.txt";
my $match;

print "$sensorAliasFile";

my @XMLfiles;
my @RRDfiles;



#find XML sonsor config files
find( sub{push @XMLfiles,$_  if (/\.xml$/)},$xmlCfgDir);

#find RRD files 
find( sub{push @RRDfiles,$_  if (/\.rrd$/)},$rrdCfgDir);

# create xml object 
my $xml = new XML::Simple;
my $rrdxml = new XML::Simple;

my $rrdxmldata = $rrdxml->XMLin("$rrdCfgDir/rrdconfig.xml");

#string used for RRDs::create
my @rrdstrings;


#Loop through XML-config files for sensors
foreach my $XMLfile (@XMLfiles)
{
	
	
	#remove .xml and path
	$XMLfile =~ s/\..*//;
	
	my $xmldata = $xml->XMLin("$xmlCfgDir/$XMLfile.xml");
	#is there a RRD file for the sensor?
	foreach my $RRDfile (@RRDfiles)
	{
		if($RRDfile =~ s/\..*// eq $XMLfile){$match = "ok";	}		
	}
	
	#No RRD file -> create one
	if ($match ne "ok")
	{
		
		$#rrdstrings =-1;
		
		print "@rrdstrings\n";
		foreach my $key (@{$rrdxmldata->{RRA}})
		{
			push @rrdstrings, "RRA:$key->{CF}:0.5:$key->{step}:$key->{rows}";
		}
		
		RRDs::create("$rrdCfgDir/$XMLfile.rrd",
				"--start","$rrdxmldata->{Global}->{'Start'}", 
				"--step",$rrdxmldata->{Global}->{'Step'}, 
				"DS:$xmldata->{alias}:$xmldata->{DST}:$xmldata->{Heartbeat}:$xmldata->{Min}:$xmldata->{Max}", 
				@rrdstrings);
		my $error = RRDs::error;
		warn "ERROR: $error\n" if $error;
	}
	
	#is the sensor defined in Alias file for OWFS?
	open FILE, "<$sensorAliasFile" or print "dead\n";
	my $aliasOK;
	while (my $line = <FILE>)
	{

		my $aliasString = "$xmldata->{Type}.$xmldata->{Id} = $xmldata->{alias}";
		print "$aliasString\n";
		
		if($line =~ m/$aliasString/)
		{
			$aliasOK = "ok";
		}
		else 
		{
			print "ko";
		}
	}
	
	if ($aliasOK eq "ok")
	{
		print "alias OK\n";
	}
	else
	{
		print "alias NOT OK\n";
	}
}



# my @sensors;
# my $defsenscnt=0;
#my %sensors = ("SensorAlias" => {name => "", temp=>99});

# my $rrdstring;

# open FH_CFG, "$cfgFileDir$sensorCfgFile" or die $!;

# my $owserver = OWNet->new('localhost:3001 -v -C');
# while (my $line = <FH_CFG>)
# {
	# $firstColon = index ("$line",":") +1;
	# $secondColon = index "$line",":",$firstColon;
	# $sensorAlias = substr("$line",$firstColon,$secondColon-$firstColon);
	# my @cfgData = split(';',$line);
	# chop(@cfgData[2]);
	# if(length($sensorAlias) != 0) 
	# {
	
		# push(@sensors, {'name'=>$sensorAlias,'temp'=>99, 'desc'=>@cfgData[2], 'deviceID' => @cfgData[1]});
		
		# $defsenscnt++;
		
	# }
# }
	# push(@sensors, {'name'=>"end",'temp'=>99});

# for(my $sensor=0;$sensor<$#sensors;$sensor++)
# {
#	foreach my $key (keys %{@sensors[$i]})
##	l�s temperaturen hvis sensoren har et device ID	
	# if(length($sensors[$sensor]{deviceID})>10)
	# {
		# $sensors[$sensor]{temp} = $owserver->read("/$sensors[$sensor]{name}/temperature");
		# print $sensors[$sensor]{name},$sensors[$sensor]{temp},"\n";
	
##		fjern mellemrum
		# $tmpString = $sensors[$sensor]{temp};
		# $tmpString =~ s/^\s+//;
		# $tmpString =~ s/\s+$//;
		
##		add to RRD update string
		# $rrdstring = $rrdstring.$tmpString.":";
	# }
	# else
	# {
		# $rrdstring = $rrdstring . "U:";
	# }	
# }
##slet sidste :
# $rrdstring = substr($rrdstring,0,length($rrdstring)-1);
# print $rrdstring,"\n";
##opdater RRD database
# RRDs::update( "$db","N:".$rrdstring);
# my $err = RRDs::error;
# print "ERROR while updating DB: $err\n" if $err;

# close FH_RRDSCR;

#generate graph
